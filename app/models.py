from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSON
from geoalchemy2 import Geometry

from .database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    auth_provider = Column(String)
    auth_sub = Column(String, index=True)
    email = Column(String, index=True)
    name = Column(String)



class WorkingZone(Base):
    __tablename__ = "working_zones"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)
    geom = Column(Geometry('MULTIPOLYGON', 4326))

    created_by_user_id = Column(ForeignKey("users.id"))
    created_by = relationship("User")


class Config(Base):
    __tablename__ = "configs"

    key = Column(String, primary_key=True, index=True)
    value = Column(JSON)

    last_modified_by_user_id = Column(ForeignKey("users.id"))
    last_modified_by = relationship("User")


class FileUpload(Base):
    __tablename__ = "file_uploads"

    id = Column(Integer, primary_key=True, index=True)
    type = Column(String)
    description = Column(String)
    license = Column(JSON)
    source = Column(String)
    url = Column(String)
    path = Column(String)
    created_by_user_id = Column(ForeignKey("users.id"))
    created_by = relationship("User")


class DataImport(Base):
    __tablename__ = "data_imports"

    id = Column(Integer, primary_key=True, index=True)
    type = Column(String)
    from_file_upload_id = Column(ForeignKey("file_uploads.id"))
    args = Column(JSON)
    status = Column(String)
    created_by_user_id = Column(ForeignKey("users.id"))
    changeset_id = Column(Integer)

    from_file_upload = relationship("FileUpload")
    created_by = relationship("User")


class DataExport(Base):
    __tablename__ = "data_exports"

    id = Column(Integer, primary_key=True, index=True)
    status = Column(String)
    planet_file_path = Column(String)
    netex_file_path = Column(String)
    created_at = Column(DateTime)
    created_by_user_id = Column(ForeignKey("users.id"))

    created_by = relationship("User")

