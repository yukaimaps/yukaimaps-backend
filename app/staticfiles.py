import os

from fastapi.staticfiles import StaticFiles as FastAPIStaticFiles
from starlette.datastructures import Headers
from starlette.responses import FileResponse
from starlette.staticfiles import NotModifiedResponse


class StaticFiles(FastAPIStaticFiles):

    def __init__(self, *args, **kwargs):
        self.as_attachment = kwargs.pop('as_attachment', False)
        super().__init__(*args, **kwargs)

    def file_response(
        self,
        full_path,
        stat_result,
        scope,
        status_code= 200,
    ):
        request_headers = Headers(scope=scope)

        if self.as_attachment:
            filename = os.path.basename(full_path)
        else:
            filename = None

        response = FileResponse(
            full_path, status_code=status_code, stat_result=stat_result,
            filename=filename,
            content_disposition_type="attachment"
        )
        if self.is_not_modified(response.headers, request_headers):
            return NotModifiedResponse(response.headers)
        return response


