import os

import wdm2netex.converter
#import readXMLFile, writeXMLFile, transformNetexIntoWDM
from . import models, schemas
from . import converters, extractors
from .database import SessionLocal
from .osm import OsmApiClient, Osmosis
from .settings import Settings
from .utils import unique_filepath

import logging; logger = logging.getLogger(__name__)


def abort(session, instance):
    logger.info("Abort task '%s'", instance.id)
    instance.status = schemas.DataImportStatus.ERROR.value
    session.add(instance)
    session.commit()



def import_data(instance_id:int, auth:str):
    logger.info("Start import task '%s'", instance_id)
    session = SessionLocal()
    instance = session.query(models.DataImport).get(instance_id)

    if not instance:
        logger.info("Import task '%s' not found", instance_id)
        return

    instance.status = schemas.DataImportStatus.RUNNING.value
    session.add(instance)
    session.commit()

    # check if an extract is needed
    extractor = extractors.registry.get(instance.type)
    if extractor:
        extent = session.query(models.Config).get("INSTANCE_EXTENT")
        if extent:
            logger.info("Extracting data for Import task '%s'", instance_id)
            filepath = extractor(extent.value)
            if not filepath:
                abort(session, instance)
                return
    else:
        filename = instance.from_file_upload.path
        filepath = os.path.join(Settings.FILE_UPLOAD_ROOT, filename)

    args = instance.args or {}

    converter = converters.registry.get(instance.type)
    if not converter:
        abort(session, instance)
        return

    logger.info("Converting data for Import task '%s'", instance_id)
    wdm = converter(filepath, args)

    osm_client = OsmApiClient(Settings.OSM_API_URL, auth)
    changeset_id = osm_client.create_changeset()
    logger.info("Created OSM changeset '%s' for Import task '%s'", changeset_id, instance_id)
    success = osm_client.create_data(changeset_id, wdm)
    logger.info("Status of Import task '%s': '%s'", instance_id, 'SUCCESS' if success else 'ERROR')
    osm_client.close_changeset(changeset_id)
    logger.info("Closed changeset '%s' for Import task '%s'", changeset_id, instance_id)

    instance.changeset_id = changeset_id
    instance.status = (
        schemas.DataImportStatus.SUCCESS.value if success
        else schemas.DataImportStatus.ERROR.value
    )
    session.add(instance)
    session.commit()


def export_data(instance_id:int, auth:str):
    logger.info("Start export task '%s'", instance_id)
    session = SessionLocal()
    instance = session.query(models.DataExport).get(instance_id)

    if not instance:
        logger.info("Export task '%s' not found", instance_id)
        return

    instance.status = schemas.DataImportStatus.RUNNING.value
    session.add(instance)
    session.commit()

    try:
        planet_file_path = unique_filepath(
            os.path.join(Settings.FILE_UPLOAD_ROOT, 'planet_wdm.osm')
        )
        logger.info("Exporting WDM '%s' file for Export task '%s'", planet_file_path, instance_id)
        Osmosis.export(planet_file_path)
        instance.planet_file_path = os.path.relpath(planet_file_path, Settings.FILE_UPLOAD_ROOT)

        netex_file_path = unique_filepath(
            os.path.join(Settings.FILE_UPLOAD_ROOT, 'planet_netex.xml')
        )

        logger.info("Exporting NeTEx '%s' file for Export task '%s'", netex_file_path, instance_id)
        wdm = wdm2netex.converter.readWdmFromXML(planet_file_path)
        wdm2netex.converter.writeWdmIntoNetex(wdm, netex_file_path)
        instance.netex_file_path = os.path.relpath(netex_file_path, Settings.FILE_UPLOAD_ROOT)
    except Exception:
        logger.exception("Error during Export task '%s'" % instance_id)
        abort(session, instance)
        return

    instance.status = schemas.DataImportStatus.SUCCESS.value
    session.add(instance)
    session.commit()


