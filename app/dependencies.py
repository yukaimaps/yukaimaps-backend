from typing import Annotated
import jwt

from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, Request
from fastapi.security import OpenIdConnect

from app import schemas, models
from app.certs import RSA_KEY
from app.settings import Settings
from app.database import SessionLocal


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def error_responses(*args):
    errors = {
        401: {"description": "Unauthorized", "model": schemas.ErrorResponse},
        404: {"description": "Not Found", "model": schemas.ErrorResponse},
    }
    return {code: errors[code] for code in args if code in errors}


oidc_scheme = OpenIdConnect(
    openIdConnectUrl=Settings.OIDC_SERVER + '/.well-known/openid-configuration'
)

def get_token_user(auth: Annotated[str, Depends(oidc_scheme)]):
    _, token = auth.split(maxsplit=1)
    try:
        user_data = jwt.decode(
            token,
            key=RSA_KEY,
            algorithms=['RS256'],
            options={'verify_aud': False}
        )
    except jwt.exceptions.InvalidTokenError as e:
        raise HTTPException(
            status_code=401,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return schemas.TokenUser(**user_data)


def get_user(
    token_user: Annotated[schemas.TokenUser, Depends(get_token_user)],
    db: Session = Depends(get_db),
):
    user = db.query(models.User).filter(
        models.User.auth_provider == token_user.iss,
        models.User.auth_sub == token_user.sub,
    ).scalar()
    if not user:
        user = models.User(
            auth_provider=token_user.iss,
            auth_sub=token_user.sub,
        )
    user.email = token_user.email
    user.name = token_user.name

    db.add(user)
    db.commit()

    return schemas.User.from_orm(user)

def get_root_path(
    request: Request,
) -> str:
    return request.scope.get("root_path", "")

