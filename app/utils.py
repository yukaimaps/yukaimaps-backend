import os

import shortuuid


def unique_filepath(path):
    new_path = path
    base, ext = os.path.splitext(path)
    while os.path.exists(new_path):
        suffix = shortuuid.uuid()[:7]
        new_path = f'{base}_{suffix}{ext}'
    return new_path


