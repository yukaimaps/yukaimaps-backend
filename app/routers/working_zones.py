from typing import Annotated

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import Response
from sqlalchemy import func

from app import schemas, models
from app.dependencies import get_user, error_responses, get_db


router = APIRouter(
    prefix="/working-zones",
    tags=["Working zones"],
)


@router.get(
    '/',
    summary="List all working zones as a GeoJSON FeatureCollection"
)
def list_working_zones(
    db: Session = Depends(get_db),
) -> schemas.WorkingZoneCollection:
    return {
        "type": "FeatureCollection",
        "features": [
            schemas.WorkingZoneFeature.parse_raw(i[0])
            for i in db.query(func.ST_AsGeoJSON(models.WorkingZone)).all()
        ]
    }


@router.get(
    '/{id}',
    responses=error_responses(404),
    summary="Get one working zone as a GeoJSON Feature",
)
def get_working_zone(
    id: int,
    db: Session = Depends(get_db),
) -> schemas.WorkingZoneFeature:
    zone = db.query(
        func.ST_AsGeoJSON(models.WorkingZone)
    ).filter(
        models.WorkingZone.id == id
    ).scalar()
    if not zone:
        raise HTTPException(status_code=404, detail="working zone not found")
    return schemas.WorkingZoneFeature.parse_raw(zone)


@router.post(
    '/',
    status_code=201,
    summary="Overwrite all working zones",
)
def import_working_zones(
    response: Response,
    collection: schemas.WorkingZoneCollection,
    current_user: Annotated[schemas.User, Depends(get_user)],
    db: Session = Depends(get_db),
):
    db.query(models.WorkingZone).delete()
    for feature in collection.features:
        db.add(models.WorkingZone(
            title=feature.properties.title,
            description=feature.properties.description,
            geom=func.ST_GeomFromGeoJSON(feature.geometry.json()),
            created_by_user_id=current_user.id,
        ))
    db.commit()
    response.headers['Location'] = '/working-zones/'
    return {}

