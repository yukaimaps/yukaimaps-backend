import os
from typing import Annotated, List

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, HTTPException, UploadFile, Form
import shortuuid

from app import schemas, models
from app.dependencies import get_user, error_responses, get_db
from app.settings import Settings
from app.utils import unique_filepath


router = APIRouter(
    prefix="/uploads",
    tags=["File uploads"],
)


def write_file(fileb):
    filename = fileb.filename

    full_path = unique_filepath(os.path.join(
        Settings.FILE_UPLOAD_ROOT,
        filename,
    ))
    with open(full_path, 'wb') as f:
        f.write(fileb.file.read())
    return os.path.relpath(full_path, Settings.FILE_UPLOAD_ROOT)


@router.get(
    '/',
    summary="List uploaded files",
)
def list_uploads(
    db: Session = Depends(get_db),
) -> List[schemas.FileUpload]:
    return db.query(models.FileUpload).all()


@router.post(
    '/',
    summary="Upload a new file",
)
def upload_file(
    current_user: Annotated[schemas.User, Depends(get_user)],
    fileb: UploadFile,
    type: Annotated[schemas.FileType, Form()],
    license_name: Annotated[str, Form()],
    license_url: Annotated[str, Form()],
    license_allow_sharing: Annotated[bool, Form()],
    license_allow_modifications: Annotated[bool, Form()],
    license_require_share_alike: Annotated[bool, Form()],
    license_require_attribution: Annotated[bool, Form()],
    db: Session = Depends(get_db),
    source: Annotated[str, Form()]="",
    description: Annotated[str, Form()]="",
    url: Annotated[str, Form()]="",
) -> schemas.FileUpload:

    filename = write_file(fileb)
    instance = models.FileUpload(
        type=type.value,
        license={
            'name': license_name,
            'url': license_url,
            'allow_sharing': license_allow_sharing,
            'allow_modifications': license_allow_modifications,
            'require_share_alike': license_require_share_alike,
            'require_attribution': license_require_attribution,
        },
        source=source,
        description=description,
        url=url,
        path=filename,
        created_by=db.query(models.User).get(current_user.id)
    )
    db.add(instance)
    db.commit()
    db.refresh(instance)
    return instance


@router.get(
    '/{id}',
    responses=error_responses(404),
    summary="Get one data import by id",
)
def get_upload(
    id: int,
    db: Session = Depends(get_db),
) -> schemas.FileUpload:
    instance = db.query(models.FileUpload).get(id)
    if not instance:
        raise HTTPException(status_code=404, detail="upload not found")
    return instance


