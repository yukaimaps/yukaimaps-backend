import os
import uuid
from typing import Annotated, List

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, HTTPException, UploadFile, Form
import shortuuid

from app import schemas, models
from app.dependencies import get_user, error_responses, get_db
from app.settings import Settings


router = APIRouter(
    prefix="/photos",
    tags=["Photos"],
)


def write_photo(fileb):
    filename = fileb.filename
    _, ext = os.path.splitext(filename)
    new_name = uuid.uuid4().hex + ext
    full_path = os.path.join(
        Settings.PHOTOS_UPLOAD_ROOT,
        new_name
    )
    with open(full_path, 'wb') as f:
        f.write(fileb.file.read())
    return new_name


@router.post(
    '/upload',
    summary="Upload new photo"
)
def upload_photo(fileb: UploadFile) -> schemas.PhotoUploadResult:
    filename = write_photo(fileb)
    return {
        'relative_url': f'/files/{filename}'
    }


