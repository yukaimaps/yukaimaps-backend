from typing import Annotated

from fastapi import APIRouter, Depends

from app import schemas
from app.dependencies import get_user, error_responses

router = APIRouter(
    prefix="",
    tags=["Root"],
)


@router.get("/", responses=error_responses(401))
async def root() -> dict:
    return {"message": "Hello World"}


@router.get("/user", responses=error_responses(401))
async def get_current_user(
    current_user: Annotated[schemas.User, Depends(get_user)],
) -> schemas.User:
    return current_user

