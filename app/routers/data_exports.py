import datetime
from typing import Annotated, List

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, HTTPException, BackgroundTasks
from fastapi.responses import Response, RedirectResponse

from app import schemas, models
from app.dependencies import (
    get_user, error_responses, oidc_scheme, get_db, get_root_path,
)
from app import tasks


router = APIRouter(
    prefix="/data-exports",
    tags=["Data exports"],
)


@router.get(
    '/',
    summary="List data exports",
)
def list_exports(
    db: Session = Depends(get_db),
) -> List[schemas.DataExport]:
    return db.query(models.DataExport).order_by(models.DataExport.id.desc()).all()


@router.post(
    '/',
    summary="Create a new data export",
    status_code=201,
)
def create_export(
    response: Response,
    auth: Annotated[str, Depends(oidc_scheme)],
    current_user: Annotated[schemas.User, Depends(get_user)],
    tasks_runner: BackgroundTasks,
    db: Session = Depends(get_db),
) -> schemas.DataExport:
    instance = models.DataExport(
        status=schemas.DataImportStatus.PENDING.value,
        created_at=datetime.datetime.now(),
        created_by=db.query(models.User).get(current_user.id)
    )
    db.add(instance)
    db.commit()
    db.refresh(instance)

    tasks_runner.add_task(tasks.export_data, instance.id, auth)

    return instance


@router.get(
    '/{id}',
    responses=error_responses(404),
    summary="Get one data export by id",
)
def get_export(
    id: int,
    db: Session = Depends(get_db),
) -> schemas.DataExport:
    instance = db.query(models.DataExport).get(id)
    if not instance:
        raise HTTPException(status_code=404, detail="data export not found")
    return instance

@router.get(
    '/{id}/files/{filename}',
    responses=error_responses(404),
    summary="Get one export file",
)
def get_export_file(
    id: int,
    filename: str,
    db: Session = Depends(get_db),
    root_path: str = Depends(get_root_path)
):
    instance = db.query(models.DataExport).get(id)
    if not instance:
        raise HTTPException(status_code=404, detail="data export not found")
    if filename not in (instance.planet_file_path, instance.netex_file_path):
        raise HTTPException(status_code=404, detail="file not found")
    staticfile_url = f'{root_path}/files/{filename}'
    return RedirectResponse(
        url=staticfile_url,
        # if nginx is there, tell to directly serve the thubmnails instead of redirect
        headers={"X-Accel-Redirect": staticfile_url}
    )





