from typing import Annotated, List

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import Response

from app import schemas, models
from app.dependencies import get_user, error_responses, get_db


router = APIRouter(
    prefix="/configs",
    tags=["Global configurations"],
)


@router.get(
    '/',
    summary="List global configurations",
)
def list_configs(
    db: Session = Depends(get_db),
) -> List[schemas.Config]:
    return db.query(models.Config).all()


@router.post(
    '/',
    summary="Create a global configuration",
    status_code=201,
)
def create_config(
    response: Response,
    config: schemas.Config,
    current_user: Annotated[schemas.User, Depends(get_user)],
    db: Session = Depends(get_db),
) -> schemas.Config:
    instance = models.Config(**dict(
        config.dict(),
        last_modified_by=db.query(models.User).get(current_user.id)
    ))
    db.add(instance)
    db.commit()
    db.refresh(instance)
    return instance


@router.get(
    '/{key}',
    responses=error_responses(404),
    summary="Get one config by key",
)
def get_config(
    key: str,
    db: Session = Depends(get_db),
) -> schemas.Config:
    config = db.query(models.Config).get(key)
    if not config:
        raise HTTPException(status_code=404, detail="config not found")
    return config


@router.put(
    '/{key}',
    responses=error_responses(404),
    summary="Update config by key",
)
def update_config(
    key: str,
    config: schemas.Config,
    current_user: Annotated[schemas.User, Depends(get_user)],
    db: Session = Depends(get_db),
) -> schemas.Config:
    instance = db.query(models.Config).get(key)
    if not instance:
        raise HTTPException(status_code=404, detail="config not found")
    instance.value = config.value
    instance.last_modified_by = db.query(models.User).get(current_user.id)
    db.add(instance)
    db.commit()
    db.refresh(instance)
    return instance


