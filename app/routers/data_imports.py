from typing import Annotated, List

from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, HTTPException, BackgroundTasks
from fastapi.responses import Response

from app import schemas, models
from app.dependencies import get_user, error_responses, oidc_scheme, get_db
from app import tasks


router = APIRouter(
    prefix="/data-imports",
    tags=["Data imports"],
)


@router.get(
    '/',
    summary="List data imports",
)
def list_imports(
    db: Session = Depends(get_db),
) -> List[schemas.DataImport]:
    return db.query(models.DataImport).all()


@router.post(
    '/',
    summary="Create a new data import",
    status_code=201,
)
def create_import(
    response: Response,
    data_import: schemas.DataImport,
    auth: Annotated[str, Depends(oidc_scheme)],
    current_user: Annotated[schemas.User, Depends(get_user)],
    tasks_runner: BackgroundTasks,
    db: Session = Depends(get_db),
) -> schemas.DataImport:
    instance = models.DataImport(**dict(
        data_import.dict(),
        type=data_import.type.value,
        status=schemas.DataImportStatus.PENDING.value,
        created_by=db.query(models.User).get(current_user.id)
    ))
    db.add(instance)
    db.commit()
    db.refresh(instance)

    tasks_runner.add_task(tasks.import_data, instance.id, auth)

    return instance


@router.get(
    '/{id}',
    responses=error_responses(404),
    summary="Get one data import by id",
)
def get_import(
    id: int,
    db: Session = Depends(get_db),
) -> schemas.DataImport:
    instance = db.query(models.DataImport).get(id)
    if not instance:
        raise HTTPException(status_code=404, detail="data import not found")
    return instance



