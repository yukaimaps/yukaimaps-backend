import datetime
from datetime import timezone
from subprocess import Popen, PIPE, STDOUT
import os
import tempfile
import json
import shutil

import requests
from lxml import etree

import osm2wdm.converter

from .utils import unique_filepath
from .settings import Settings

import logging; logger = logging.getLogger(__name__)


OSM_FRANCE_EXTRACTS_URL = "https://download.openstreetmap.fr/extracts/europe/france/"

OSM_FRANCE_EXTRACTS = {
    "67": "/alsace/bas_rhin.osm.pbf",
    "68": "/alsace/haut_rhin.osm.pbf",

    "24": "/aquitaine/dordogne.osm.pbf",
    "33": "/aquitaine/gironde.osm.pbf",
    "40": "/aquitaine/landes.osm.pbf",
    "47": "/aquitaine/lot_et_garonne.osm.pbf",
    "64": "/aquitaine/pyrenees_atlantiques.osm.pbf",

    "03": "/auvergne/allier.osm.pbf",
    "15": "/auvergne/cantal.osm.pbf",
    "43": "/auvergne/haute_loire.osm.pbf",
    "63": "/auvergne/puy_de_dome.osm.pbf",

    "01": "/rhone_alpes/ain.osm.pbf",
    "02": "/picardie/aisne.osm.pbf",
    "17": "/poitou_charentes/charente_maritime.osm.pbf",
    "13": "/provence_alpes_cote_d_azur/bouches_du_rhone.osm.pbf",
    "39": "/franche_comte/jura.osm.pbf",
    "56": "/bretagne/morbihan.osm.pbf",
    "69": "/rhone_alpes/rhone.osm.pbf",
    "74": "/rhone_alpes/haute_savoie.osm.pbf",
    "77": "/ile_de_france/seine_et_marne.osm.pbf",
    "06": "/provence_alpes_cote_d_azur/alpes_maritimes.osm.pbf",
    "83": "/provence_alpes_cote_d_azur/var.osm.pbf",
    "84": "/provence_alpes_cote_d_azur/vaucluse.osm.pbf",
}



def ordered_wdm_elements(wdm, changeset_id=None, version=None, timestamp=None):
    tags = ('node', 'way', 'relation')
    for tag in tags:
        for element in wdm.xpath(f'//{tag}'):
            if timestamp:
                element.set('timestamp', timestamp)
            if changeset_id:
                element.set('changeset', changeset_id)
            if version:
                element.set('version', version)
            yield element

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

class OsmApiClient:

    def __init__(self, base_url, auth):
        self.base_url = base_url
        self.auth = auth

    def _call_api(self, method, endpoint, xml=False, headers=None, **kwargs):
        base_headers = {'Authorization': self.auth}
        if xml:
            base_headers.update({ 'Content-Type': 'text/xml' })
        if headers is not None:
            base_headers.update(headers)
        return requests.request(
            method=method,
            url=self.base_url + endpoint,
            headers=base_headers,
            **kwargs
        )

    def create_changeset(self):
        payload = (
            '<?xml version="1.0" encoding="UTF-8"?>'
            '<osm>'
                '<changeset>'
                    '<tag k="comment" v="Import"/>'
                '</changeset>'
            '</osm>'
        )
        resp = self._call_api('PUT', '/changeset/create', data=payload, xml=True)
        if resp.status_code == 200:
            return resp.text
        return None

    def create_data(self, changeset_id, wdm):
        now = datetime.datetime.now(timezone.utc).isoformat()
        nodes = ordered_wdm_elements(
            wdm, changeset_id, version="1", timestamp=now
        )
        nodes_str = ''.join(etree.tostring(e, encoding=str) for e in nodes)
        payload = (
            '<?xml version="1.0" encoding="UTF-8"?>'
            '<osmChange version="0.6">'
                '<create>'
                    f'{nodes_str}'
                '</create>'
            '</osmChange>'
        )
        resp = self._call_api(
            'POST',
            f'/changeset/{changeset_id}/upload',
            data=payload.encode('utf-8'),
            xml=True,
        )
        return resp.status_code == 200

    def close_changeset(self, changeset_id):
        resp = self._call_api('PUT', f'/changeset/{changeset_id}/close')
        return resp.status_code == 200


class Osmosis:

    executable = '/usr/local/bin/osmosis'
    db = (Settings.OSMOSIS_DB_CONNECTION_STRING + ' validateSchemaVersion=no').split()

    @classmethod
    def run(cls, in_, *args):
        cmd_args = (
            cls.executable,
        ) + args
        cmd = Popen(cmd_args, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        out, _ = cmd.communicate(in_)
        if cmd.returncode != 0:
            raise RuntimeError(out)
        return out.decode()

    @classmethod
    def truncate(cls):
        return cls.run(None, '--truncate-apidb', *cls.db)

    @classmethod
    def apply_change(cls, change):
        return cls.run(
            change,
            '--read-xml-change',
            f'file=-',
            '--write-apidb-change',
            *cls.db
        )

    @classmethod
    def export(cls, output_file):
        return cls.run(*[
            None,
            '--read-apidb-current',
        ] + cls.db + [
            '--write-xml',
            output_file
        ])

    @classmethod
    def merge(cls, in_files, out_file):
        args = []
        for i in in_files:
            args.append('--rb')
            args.append(i)
        for _ in range(len(in_files) - 1):
            args.append('--merge')
        args.append('--wb')
        args.append(out_file)
        return cls.run(None, *args)


class Osmium:

    executable = '/usr/bin/osmium'

    @classmethod
    def run(cls, command, *args):
        cmd_args = (
            cls.executable,
            command,
        ) + args
        cmd = Popen(cmd_args, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        out, _ = cmd.communicate()
        if cmd.returncode != 0:
            raise RuntimeError(out)
        return out.decode()

    @classmethod
    def merge(cls, in_files, out_file):
        return cls.run('merge', *in_files, '-o', out_file)

    @classmethod
    def sort(cls, in_file, out_file):
        return cls.run('sort', in_file, '-o', out_file)

    @classmethod
    def extract(cls, in_file, geojson_file, out_file):
        return cls.run('extract', '-p', geojson_file, in_file, '-o', out_file)

    @classmethod
    def filter(cls, in_file, out_file):
        filters = osm2wdm.converter.getOsmiumFilter().split(' ')
        return cls.run(
            'tags-filter',
            in_file,
            '-o',
            out_file,
            *filters
        )


def create_osm_extract(extent):
    departements = extent.get('properties', {}).get('codesDepartements', [])
    if not departements:
        departements = [extent.get('properties', {}).get('codeDepartement')]
    with tempfile.TemporaryDirectory() as tmpdirname:
        with open(os.path.join(tmpdirname, 'extent.geojson'), 'w') as f:
            json.dump(extent, f)
        for departement in departements:
            url = OSM_FRANCE_EXTRACTS_URL + OSM_FRANCE_EXTRACTS[departement]
            r = requests.get(url)
            with open(os.path.join(tmpdirname, f'{departement}.osm.pbf'), 'wb') as f:
                f.write(r.content)
        if len(departements) == 1:
            Osmium.sort(
                os.path.join(tmpdirname, f'{departements[0]}.osm.pbf'),
                os.path.join(tmpdirname, 'sorted.osm.pbf'),
            )
        else:
            Osmosis.merge(
                [os.path.join(tmpdirname, f'{d}.osm.pbf') for d in departements],
                os.path.join(tmpdirname, 'merged.osm.pbf')
            )
            Osmium.sort(
                os.path.join(tmpdirname, 'merged.osm.pbf'),
                os.path.join(tmpdirname, 'sorted.osm.pbf'),
            )

        Osmium.extract(
            os.path.join(tmpdirname, 'sorted.osm.pbf'),
            os.path.join(tmpdirname, 'extent.geojson'),
            os.path.join(tmpdirname, 'extract.osm.pbf'),
        )
        upload_file_path = unique_filepath(os.path.join(Settings.FILE_UPLOAD_ROOT, 'extent.osm'))
        Osmium.filter(
            os.path.join(tmpdirname, 'extract.osm.pbf'),
            os.path.join(tmpdirname, upload_file_path),
        )
        return upload_file_path

