import os
from fastapi import Depends, FastAPI, APIRouter
from fastapi.middleware.cors import CORSMiddleware

from app.staticfiles import StaticFiles
from app.dependencies import get_token_user, error_responses
from app.routers import (
    root,
    configs,
    working_zones,
    uploads,
    data_imports,
    data_exports,
    photos,
)
from app.settings import Settings



app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.mount(
    "/files",
    StaticFiles(directory=Settings.FILE_UPLOAD_ROOT, as_attachment=True),
    name="photos"
)
app.mount(
    "/photos/files",
    StaticFiles(directory=Settings.PHOTOS_UPLOAD_ROOT),
    name="photos"
)

public_router = APIRouter(
    prefix="",
)

private_router = APIRouter(
    prefix="",
    dependencies=[Depends(get_token_user)],
    responses=error_responses(401),
)

public_router.include_router(root.router)

private_router.include_router(configs.router)
private_router.include_router(working_zones.router)
private_router.include_router(uploads.router)
private_router.include_router(data_imports.router)
private_router.include_router(data_exports.router)
private_router.include_router(photos.router)

app.include_router(public_router)
app.include_router(private_router)




# ------------------------------------------------------------------------------
# Working zones
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# Global configurations
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# File uploads
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Data imports
# ------------------------------------------------------------------------------
