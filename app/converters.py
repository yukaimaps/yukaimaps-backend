import netex2wdm.converter
import osm2wdm.converter

from . import schemas


def netex_converter(filepath, args={}):
    netex = netex2wdm.converter.readXMLFile(filepath)
    wdm = netex2wdm.converter.transformNetexIntoWDM(netex)
    return wdm

def osm_converter(filepath, args={}):
    data_types = args.get('data_types', [])
    osm = osm2wdm.converter.readXMLFile(filepath)
    wdm = osm2wdm.converter.transformOsmIntoWDM(osm, data_types)
    return wdm

def osm_transport_stops_converter(filepath, args={}):
    return osm_converter(filepath, 'transport')

def osm_poi_converter(filepath, args={}):
    return osm_converter(filepath, 'poi')

def osm_paths_converters(filepath, args={}):
    return osm_converter(filepath, ['path'])


registry = {
    schemas.DataImportType.NETEX_TRANSPORT_STOPS.value: netex_converter,
    schemas.DataImportType.OSM_TRANSPORT_STOPS.value: osm_transport_stops_converter,
    schemas.DataImportType.OSM_POI.value: osm_poi_converter,
    schemas.DataImportType.OSM_PATHS.value: osm_paths_converters,
    schemas.DataImportType.OSM.value: osm_converter,
}

