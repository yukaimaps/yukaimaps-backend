from . import schemas
from .osm import create_osm_extract
import logging; logger = logging.getLogger(__name__)



def osm_extractor(extent):
    if not extent:
        return
    try:
        filepath = create_osm_extract(extent)
    except Exception:
        logger.exception('error extracting data')
        return
    return filepath


registry = {
    schemas.DataImportType.OSM.value: osm_extractor,
}

