import os


class Settings:

    CERTS_FILE_PATH = os.environ.get(
        "CERTS_FILE_PATH",
        None
    )

    FILE_UPLOAD_ROOT = os.environ.get(
        "FILE_UPLOAD_ROOT",
        os.path.join(os.getcwd(), 'files')
    )

    PHOTOS_UPLOAD_ROOT = os.environ.get(
        "PHOTOS_UPLOAD_ROOT",
        os.path.join(os.getcwd(), 'photos')
    )

    OSM_API_URL = os.environ.get(
        'OSM_API_URL',
        'http://localhost:3000/api/0.6'
    )

    OIDC_SERVER = os.environ.get(
        'OIDC_SERVER',
        'http://localhost:3030/realms/yukaimaps',
    )

    SQLALCHEMY_DATABASE_URL = os.environ.get(
        'SQLALCHEMY_DATABASE_URL',
        "postgresql://jerome:passe@localhost/yukaimaps"
    )

    OSMOSIS_DB_CONNECTION_STRING = os.environ.get(
        'OSMOSIS_DB_CONNECTION_STRING',
        'host=db user=openstreetmap database=openstreetmap password=openstreetmap'
    )
