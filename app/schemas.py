from datetime import datetime
from enum import Enum
from typing import Any, List, Optional

from pydantic import BaseModel, Field, root_validator


class ErrorResponse(BaseModel):
    detail: str



class TokenUser(BaseModel):
    iss: str
    sub: str
    email: str
    name: str



class User(BaseModel):
    id: int
    auth_provider: str = ""
    auth_sub: str = ""
    email: str = ""
    name: str = ""

    class Config:
        orm_mode = True


class FeatureType(Enum):
    Feature = "Feature"

class FeatureCollectionType(Enum):
    FeatureCollection = "FeatureCollection"

class GeomType(Enum):
    MultiPolygon = 'MultiPolygon'


class Coordinate(BaseModel):
    __root__: List[Any]


class GeojsonMultipolygon(BaseModel):
    type: GeomType
    coordinates: List[List[List[Coordinate]]]


class WorkingZoneProperties(BaseModel):
    id: Optional[int]
    title: str
    description: str = ""
    created_by_user_id: Optional[int]


class WorkingZoneFeature(BaseModel):
   type: FeatureType
   properties: WorkingZoneProperties
   geometry: GeojsonMultipolygon


class WorkingZoneCollection(BaseModel):
    type: FeatureCollectionType
    features: List[WorkingZoneFeature]


class Config(BaseModel):
    key: Optional[str]
    value: dict = {}
    last_modified_by: Optional[User]

    class Config:
        orm_mode = True


class FileType(Enum):
    NETEX = 'NETEX'
    OSM = 'OSM'


class DataLicense(BaseModel):
    name: str
    url: Optional[str]
    allow_sharing: bool
    allow_modifications: bool
    require_share_alike: bool
    require_attribution: bool

class FileUpload(BaseModel):
    id: Optional[int]
    type: FileType
    description: str = ""
    license: DataLicense
    source: str = ""
    url: str = ""
    path: str
    created_by: Optional[User]

    class Config:
        orm_mode = True


class DataImportType(Enum):
    NETEX_TRANSPORT_STOPS = 'NETEX_TRANSPORT_STOPS'
    OSM = 'OSM'
    OSM_POI = 'OSM_POI'
    OSM_PATHS = 'OSM_PATHS'
    OSM_TRANSPORT_STOPS = 'OSM_TRANSPORT_STOPS'


class DataImportStatus(Enum):
    PENDING = 'PENDING'
    RUNNING = 'RUNNING'
    SUCCESS = 'SUCCESS'
    ERROR = 'ERROR'


class DataImport(BaseModel):
    id: Optional[int]
    type: DataImportType
    from_file_upload_id: Optional[int]
    args: Optional[dict]
    status: Optional[DataImportStatus] = DataImportStatus.PENDING
    changeset_id: Optional[int]
    created_by: Optional[User]

    @root_validator()
    def check_upload_ommitted(cls, values):
        if not values.get('from_file_upload_id'):
            if values.get('type') != DataImportType.OSM:
                raise ValueError('Missing file upload id for this import type')
        return values

    class Config:
        orm_mode = True


class PhotoUploadResult(BaseModel):
    relative_url: str


class DataExport(BaseModel):
    id: Optional[int]
    status: Optional[DataImportStatus] = DataImportStatus.PENDING
    planet_file_path: Optional[str]
    netex_file_path: Optional[str]
    created_at: Optional[datetime]
    created_by: Optional[User]

    class Config:
        orm_mode = True

