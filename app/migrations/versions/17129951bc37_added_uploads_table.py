"""Added uploads table

Revision ID: 17129951bc37
Revises: a2472d446624
Create Date: 2023-04-25 16:17:42.046922

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '17129951bc37'
down_revision = 'a2472d446624'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('file_uploads',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('type', sa.String(), nullable=True),
    sa.Column('description', sa.String(), nullable=True),
    sa.Column('license', postgresql.JSON(astext_type=sa.Text()), nullable=True),
    sa.Column('source', sa.String(), nullable=True),
    sa.Column('url', sa.String(), nullable=True),
    sa.Column('path', sa.String(), nullable=True),
    sa.Column('created_by_user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['created_by_user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_file_uploads_id'), 'file_uploads', ['id'], unique=False)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_file_uploads_id'), table_name='file_uploads')
    op.drop_table('file_uploads')
    # ### end Alembic commands ###
