"""Add working zones

Revision ID: 1ff2b8cb3c0f
Revises: 
Create Date: 2023-04-04 16:13:56.300702

"""
from alembic import op
import sqlalchemy as sa
from geoalchemy2 import Geometry

# revision identifiers, used by Alembic.
revision = '1ff2b8cb3c0f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_geospatial_table('working_zones',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(), nullable=True),
    sa.Column('description', sa.String(), nullable=True),
    sa.Column('geom', Geometry(geometry_type='MULTIPOLYGON', spatial_index=False, from_text='ST_GeomFromEWKT', name='geometry'), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_geospatial_index('idx_working_zones_geom', 'working_zones', ['geom'], unique=False, postgresql_using='gist', postgresql_ops={})
    op.create_index(op.f('ix_working_zones_description'), 'working_zones', ['description'], unique=False)
    op.create_index(op.f('ix_working_zones_id'), 'working_zones', ['id'], unique=False)
    op.create_index(op.f('ix_working_zones_title'), 'working_zones', ['title'], unique=False)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_working_zones_title'), table_name='working_zones')
    op.drop_index(op.f('ix_working_zones_id'), table_name='working_zones')
    op.drop_index(op.f('ix_working_zones_description'), table_name='working_zones')
    op.drop_geospatial_index('idx_working_zones_geom', table_name='working_zones', postgresql_using='gist', column_name='geom')
    op.drop_geospatial_table('working_zones')
    # ### end Alembic commands ###
