"""Added changeset_id column in data import

Revision ID: 24c5eb8fdf34
Revises: 744c08180259
Create Date: 2023-04-27 15:50:36.943143

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '24c5eb8fdf34'
down_revision = '744c08180259'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('data_imports', sa.Column('changeset_id', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('data_imports', 'changeset_id')
    # ### end Alembic commands ###
