Yukaimaps Backend
=================

Rest API built with Python and FastAPI


Install
-------

You need a PostgreSQL database with PostGIS

```
createdb yukaimaps
psql -d yukaimaps -c "create extension postgis;"
```

In a Python virtualenv (3.10+):

```
pip install -r requirements.txt
```

Configure app witj environment variables:

```
export SQLALCHEMY_DATABASE_URL=postgresql://user:password@localhost/yukaimaps
```

See other possibles configurations in [`app/settings.py`](app/settings.py)

Run the app in dev mode

```
uvicorn app.main:app --reload
```



