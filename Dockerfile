from python:3.11-bookworm

ENV PYTHONDONTWRITEBYTECODE 1

RUN apt-get -y update && \
    apt-get install --no-install-recommends -y \
    osmium-tool \
    default-jre-headless

# install osmosis
RUN mkdir -p /opt/osmosis
RUN wget -qO- https://github.com/openstreetmap/osmosis/releases/download/0.49.2/osmosis-0.49.2.tar | tar xv -C /opt/osmosis
RUN ln -s /opt/osmosis/osmosis-0.49.2/bin/osmosis /usr/local/bin/osmosis

# install python app
RUN mkdir -p /opt/app

WORKDIR /opt/app

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . ./


